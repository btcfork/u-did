# U-DID

"Uncoerced Donations via Information Diffusion"

A voluntary funding system (protocol) built on top of Bitcoin Cash
(or similar blockchains).

Please have a look at the following descriptive articles about it:

- [Introductory concept post](https://read.cash/@btcfork/u-did-what-3ee6cc03)
- [Overview diagram for v1 (brainstorming)](https://read.cash/@btcfork/u-did-overview-diagram-v1-brainstorm-6e28f505)
- [Demo, part 1 (intent)](https://read.cash/@btcfork/u-did-demo-part-1-e31c6ca3)
- [Demo, part 2 (fulfilment)](https://read.cash/@btcfork/u-did-demo-part-2-fulfilment-a8f727e5)

A more or less loose specification of the protocol messages is currently
under development in this repository (see spec/ folder).

Your input is welcome in the form of Issues, Pull Requests or comments on
the development direction in any of the following locations:

- on read.cash, as comments on any related posts or in the [forum discussion on
  v1](https://read.cash/forum/bitcoin-cash/discussion-of-u-did-protocol-version-1-things-99646888)
- on memo.cash, in the ['U-DID' topic](https://memo.cash/topic/U-DID)
