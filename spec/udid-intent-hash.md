# udid-intent-hash

## Description

This on chain message provides a document hash and source location for
an intent message that may be located on or off chain.

NOTE: corresponding document message: [udid-intent](udid-intent.md)


## Deposition requirements

Hash message must always be on chain.


## Fields

| Field Name    | Type          | Description   | mandatory or optional | Values |
| ---           | ---           | ---           | ---                   | ---    |
| version       | integer       | U-DID version | mandatory             | 1      |
| type          | string        | hash algorithm| mandatory      | algorithm identifier e.g. "sha224", "sha256", "sha384", "sha512" |
| value         | string        | digest of referenced intent doc | mandatory      | string representation of digest as hexadecimal value |
| source        | object | provides reference to intent | mandatory | possible contents are: txid, url |
| source.txid   | string        | transaction storing intent (if on-chain)| optional, but must be specified iff no source.url is given | transaction id (in hexadecimal string representation) |
| source.url    | string        | URL storing intent (if off-chain)| optional, but must be specified iff no source.txid is given | URL which provides intent document |


## Semantic Validity

1. TBD


## Additional notes

TBD


## Examples

Below is a simple example showing a intent hash pointing to a
intent that is located off chain at the URL referred
to by the "url" attribute.

```javascript
{
  "udid-intent-hash" : {
    "version" : 1,
    "type" : "sha256",
    "value" : "956baf087db15d0665b442f2d07999db76e23e8cd6c4ff7bea8067395ae54685",
    "source" : {
      "url" : "https://gitlab.com/btcfork/u-did/-/raw/master/spec/examples/btcfork_intent2.txt"
    }
   }
}
```
