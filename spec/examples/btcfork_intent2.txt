{
	"udid-intent": {
		"version": 1,
		"total_amount": 1.0,
		"currency_unit": "BCH",
		"recipients": [{
				"name": "Bitcoin ABC",
				"percentage": 20
			},
			{
				"name": "Bitcoin Unlimited",
				"percentage": 20
			},
			{
				"name": "FloweeTheHub",
				"percentage": 20
			},
			{
				"name": "BCHD",
				"percentage": 20
			},
			{
				"name": "Bitcoin Verde",
				"percentage": 20
			}
		]
	}
}
