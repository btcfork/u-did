# udid-bouncy-announcement

## Description

This message announces a bounty for completion of one or more
specified tasks.

Bounties can be of the following types:

- contest: several workers compete to win (or split) a bounty
- single: an applicant is selected to do the work
- cooperative: multiple workers do the work, no "winner"

Bounties can be permissionless or require approval of applicants
from the originator.

The originator may optionally choose to provide identifying information.

Contest awards are described by amount or percentage of the bounty
paid to each rank (first place, second place etc).

For single or cooperative bounties, no payout structure needs to be
specified. Single winners normally earn the whole bounty, and
in the case of a cooperative bounty, the originator can freely
determine how to split the total amount over the workers (as not
everyone may contribute the same work to the overall effort).


## Deposition requirements

- message may be on chain if short enough: YES
- message may be off chain: YES
- message requires corresponding hash message if on chain: NO
- message requires corresponding hash message if off chain: YES

NOTE: corresponding hash message: [udid-bounty-announcement-hash](udid-bounty-announcement-hash.md)


## Fields

| Field Name    | Type          | Description   | mandatory or optional | Values |
| ---           | ---           | ---           | ---                   | ---    |
| version       | integer       | U-DID version | mandatory             | 1      |
| total_amount  | decimal       | funding amount| mandatory      | > 0.0  |
| currency_unit | string        | currency unit | mandatory      | e.g. "BCH", "USD" |
| bounty_type   | string        | bounty type   | mandatory      | "contest", "single" or "cooperative" |
| permissions   | string        | permission    | optional      | "permissionless" if open to anyone to start the work, "approved" if submitters have to apply and be approved for eligibility |
| objective     | string        | bounty objective | mandatory      | simple text description of bounty objective|
| more_info     | string        | additional bounty info | optional | some link to additional bounty info, can be an URL or some other location descriptor |
| expires       | string        | bounty expire timestamp | mandatory | UNIX epoch timestamp (resolution: seconds) of expiry time|
| winner_payouts| array of {rank, percentage} or {rank,amount} | bounty award structure | optional: needed for "contest" bounties, not otherwise | if present, must contain at least one entry |
| rank          | integer       | winner rank   | mandatory     | natural number ranking the winner, starting atrank 1. If there are multiple winners/runners-up, then rank 2, 3, ... can be specified |
| percentage    | decimal       | percentage of total_amount awarded to winner of a particular rank | mandatory if awards are specified as percentages | between 0.0 and 100.0 (all percentages must add up to 100 |
| amount        | decimal       | amount of total awarded to winner of a particular rank | mandatory if awards are specified as amounts | > 0.0  |
| origin        | string        | identification of originator | optional | any identifying information the bountty originator wishes to provide. Contact details should be given if the bounty is not permissionless |

## Semantic Validity

1. The total amount must be specified and greater than 0.

2. if bounty_type is "contest", then "winner_payouts" is required

3. if bounty_type is "contest", then "winner_payouts" must either be specified entirely in percentages (which must add up to 100), or in amounts which must add up to the total_amount

4. if bounty_type is not "contest", then "winner_payouts" are not needed and should be ignored

5. expired bounties can be ignored but some sites may want to still display them


## Additional notes

Some sites may want to still display expired bounties.

Do not make validity assumptions about the "origin" value.
This field could be abused by fake submitters, so it is best to apply
additional trust considerations (e.g. based on an address that submitted
the bounty).

Those contacting an originator should take measures to protect their
systems' integrity against those who already abuse existing communication
services to spread malware etc.


## Examples

Below is a simple example of a contest bounty.

```javascript
{
  "udid-bounty-announcement": {
    "version": 1,
    "objective": "project logo for U-DID",
    "total_amount": 0.1,
    "currency_unit": "BCH",
    "bounty_type": "contest",
    "winner_payouts": [
      {
        "rank": 1,
        "percentage": 100
      }
    ],
    "expires": 1582372800
  }
}
```
