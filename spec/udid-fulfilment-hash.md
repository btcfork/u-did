# udid-fulfilment-hash

## Description

This on chain message provides a document hash and source location for
a fulfilment message that may be located on or off chain.

NOTE: corresponding document message: [udid-fulfilment](udid-fulfilment.md)


## Deposition requirements

Hash message must always be on chain.


## Fields

| Field Name    | Type          | Description   | mandatory or optional | Values |
| ---           | ---           | ---           | ---                   | ---    |
| version       | integer       | U-DID version | mandatory             | 1      |
| type          | string        | hash algorithm| mandatory      | algorithm identifier e.g. "sha224", "sha256", "sha384", "sha512" |
| value         | string        | digest of referenced fulfilment doc | mandatory      | string representation of digest as hexadecimal value |
| source        | object | provides reference to fulfilment | mandatory | possible contents are: txid, url |
| source.txid   | string        | transaction storing fulfilment (if on-chain)| optional, but must be specified iff no source.url is given | transaction id (in hexadecimal string representation) |
| source.url    | string        | URL storing fulfilment (if off-chain)| optional, but must be specified iff no source.txid is given | URL which provides fulfilment document |


## Semantic Validity

1. TBD


## Additional notes

TBD


## Examples

Below is a simple example showing a fulfilment hash pointing to a
fulfilment that is located on chain at the transaction id referred
to by the "txid" attribute.

```javascript
{
  "udid-fulfilment-hash" : {
    "version" : 1,
    "type" : "sha256",
    "value" : "35020a9374c48810d80ba81bd68b652c260accf037e373432ff94acc8c03966a",
    "source" : {
      "txid" : "83c4249b903c02224bc832a273bdc9e106f725e697ea910276f3ca219afdcf5a"
    }
  }
}
```
