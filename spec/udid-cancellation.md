# udid-cancellation

This message can be used to cancel prior intents, bounties or
funding requests.

Can be on chain: YES
Can be off chain: YES
Requires hash if on chain: NO

Fields:

version
cancels

