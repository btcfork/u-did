# udid-funding-request

This message is a request for funding in order to perform some
specified work.

Can be on chain: YES
Can be off chain: YES
Requires hash if on chain: NO

Fields:

version
total_amount
currency_unit
description
deliverables
milestones
schedule
benefits
risks
uncertainties
dependencies
restrictions
constraints

