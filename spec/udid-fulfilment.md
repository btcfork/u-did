# udid-fulfilment

## Description

This message documents the provision of funding corresponding
to a previously published intent.

The sender may optionally choose to provide identifying information.

## Deposition requirements

- message may be on chain if short enough: YES
- message may be off chain: YES
- message requires corresponding hash message if on chain: NO
- message requires corresponding hash message if off chain: YES

NOTE: corresponding hash message: [udid-fulfilment-hash](udid-fulfilment-hash.md)

## Fields

| Field Name    | Type          | Description   | mandatory or optional | Values |
| ---           | ---           | ---           | ---                   | ---    |
| version       | integer       | U-DID version | mandatory             | 1      |
| intent        | string        | reference to the intent fulfilled | mandatory      | transaction ID of intent-hash or intent (if on-chain) |
| payment_txs   | array of strings | reference to transactions fulfilling the intent | mandatory | transaction IDs |
| sender        | string        | identification of sender | optional     | any identifying information the sender wishes to provide |


## Semantic Validity

1. A fulfilment shall be considered invalid until its referenced intent transaction is confirmed on the blockchain.

2. The intent shall match a confirmed transaction ID that resolves to a U-DID intent (either via an intent-hash, or via some form of udid-intent being stored directly on chain).

3. A fulfilment shall not be considered valid until its referenced payment_txs are confirmed on the blockchain.

4. Whether a fulfilment adequately covers, via its referenced payment transaction outputs, the amount originally intended, shall be left to the recipient to decide.

5. Identifying information should always be treated as unconfirmed and possibly misleading unless sender proved identity by a signed message in this field.


## Additional notes

1. Ascertaining whether an intent has been completely fulfilled is difficult to tell in the general case.

   For simple cases, e.g. only one payment_txs with an output corresponding exactly to the intended total_amount, it is quite easy to agree.

   If the intent specified an amount in a currency other than the native one (e.g. BCH) or a quantity of tokens on it, it becomes more difficult to say, due to exchange rate fluctuations.

   Users might also want to create fulfilment messages which describe partial payments relating to a prior intent. However, this is discouraged - it is preferable to cancel the original intent with a reason stating that only partial payment is able to be made, and creating a corresponding new intent with reduced amount, then fulfilling that one completely.



## Examples

Below is a simple example showing fulfilment of an actual intent.
It also illustrates that the order of fields does not need to match
the order listed above (e.g. the "version" field in the example is
at the end).

```javascript
{
  "udid-fulfilment" : {
    "intent" : "f3c497006417c43a00362486a1766dc271e9190c11c41f425808ce056dda3951",
    "payment_txs" : [
      "3cabf982a4ff85393ff2fc9a07f2770ea20388822d5dcd099358431f10d83cf0"
    ],
    "sender" : "btcfork",
    "version" : 1
  }
}
```
