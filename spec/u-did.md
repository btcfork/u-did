# U-DID ("Uncoerced Donations via Information Diffusion")

A voluntary funding system (protocol) built on top of Bitcoin Cash
(or similar blockchains).


## Description

TBC


## Participants

- investors
- developers
- general public


## Messages

The basic protocol messages which are described in following
subsections are:

- intents
- fulfilments
- bounty-announcements
- funding-requests
- cancellations
- hashes (intent-hash, fulfilment-hash, funding-request-hash,
  bounty-announcement-hash, cancellation-hash)


## Hash messages

Hash messages are used to notarize longer protocol messages which
may not fit into a single data carrier field of the underlying
blockchhain (e.g. into a single OP_RETURN).

Hash messages provide a cryptographic digest (checksum) of the
longer message, and point to where a user may locate it.



## Intent

This message indicate the intention to provide funding to some
project or cause.


## Fulfilment

This message documents the provision of funding corresponding
to a previously published intent.


## Bounty-announcement

This message announces a bounty for completion of one or more
specified tasks.

Bounties can take various forms

- open contests where several entrants compete by doing the work
- selection among applicants to determine who may perform the work


## Funding-request

This message is a request for funding in order to perform some
specified work.


## Cancellation

This message can be used to cancel prior intents, bounties or
funding requests.
