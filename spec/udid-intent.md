# udid-intent

## Description

This message indicates the intention to provide funding to some
projects or causes.

The total amount must be specified, and must exceed zero.

Recipient funding can be described either in terms of percentages, or in
terms of absolute amount values.

A list of conditions on the intended funding can be given.
The assumption is that if any of the described conditions are not met,
there should not be any expectation of fulfilment of the intended funding.

Optionally, the originator of the intent can provide proof of funds
and a contact address for further discussion.

NOTE: corresponding hash message: [udid-intent-hash](udid-intent-hash.md)

## Deposition requirements

- message may be on chain if short enough: YES
- message may be off chain: YES
- message requires corresponding hash message if on chain: NO
- message requires corresponding hash message if off chain: YES

NOTE: corresponding hash message: udid-intent-hash

## Fields

| Field Name    | Type          | Description   | mandatory or optional | Values |
| ---           | ---           | ---           | ---            | ---    |
| version       | integer       | U-DID version | mandatory      | 1      |
| total_amount  | decimal       | funding amount| mandatory      | > 0.0  |
| currency_unit | string        | currency unit | mandatory      | e.g. "BCH", "USD" |
| recipients    | array of {name, percentage} or {name,amount} | list of recipients | mandatory | at least one entry |
| name          | string        | recipient name | mandatory     | non-empty, should at least allow recipient(s) to identify themselves |
| percentage    | decimal       | percentage of total_amount allocated to recipient | mandatory if allocations are specified as percentages | between 0.0 and 100.0 (all percentages must add up to 100 |
| amount        | decimal       | amount of total allocated to recipient | mandatory if allocations are specified as amounts | > 0.0  |
| conditions    | array of {condition_number, condition_description} | list of conditions to be met for the inteded funding to be fulfilled | optional | if conditions present, shall contain at least one entry |
| condition_number      | integer       | condition number | mandatory if conditions present | unique natural number >= 1 (advisable to use natural numbers 1,2,3,...) |
| condition_description     | string        | condition description | mandatory if conditions present | non-empty, unique |
| funds_proof | string        | proof of funds | optional      | valid signature proving sufficient funds |
| contact | string        | contact address | optional      | an address where people can contact to discuss about this intent |


## Semantic Validity

1. The total amount must be specified and greater than 0.

2. Percentages and absolute amounts cannot be mixed in the recipients array.
   Either one or the other must be used.

3. If percentages are used, they must add up to exactly 100.

4. If amounts are used, they must add up exactly to the total_amount.

5. If the conditions array is present, it shall contain at least one entry.

6. If the conditions array is present, the conditions shall be numbered with
   distinct condition_number values (no duplicates).

7. If the conditions array is present, there shall not be any duplicate
   condition_description values or condition_description which are empty strings.


## Additional notes

Do not assume that a given contact address belongs to the originator.
This field could be abused by fake intents, so it is best to apply
additional trust considerations based on knowing the originator or at
least seeing proof of funds.

Those contacting should take measures to protect their systems' integrity
against those who already abuse existing communication services to spread
malware etc.


## Examples

Below is a simple example that distributes an
amount to some BCH development teams (by percentages).

There are no conditions.

```javascript
{
  "udid-intent": {
    "version": 1,
    "total_amount": 1.0,
    "currency_unit": "BCH",
    "recipients": [{
        "name": "Bitcoin ABC",
        "percentage": 20
      },
      {
        "name": "Bitcoin Unlimited",
        "percentage": 20
      },
      {
        "name": "FloweeTheHub",
        "percentage": 20
      },
      {
        "name": "BCHD",
        "percentage": 20
      },
      {
        "name": "Bitcoin Verde",
        "percentage": 20
      }
    ]
  }
}
```

The above could equivalently be expressed with amounts instead of percentages:

```javascript
{
  "udid-intent": {
    "version": 1,
    "total_amount": 1.0,
    "currency_unit": "BCH",
    "recipients": [{
        "name": "Bitcoin ABC",
        "amount": 0.2
      },
      {
        "name": "Bitcoin Unlimited",
        "amount": 0.2
      },
      {
        "name": "FloweeTheHub",
        "amount": 0.2
      },
      {
        "name": "BCHD",
        "amount": 0.2
      },
      {
        "name": "Bitcoin Verde",
        "amount": 0.2
      }
    ]
  }
}
```

